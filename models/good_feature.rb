class GoodFeature < ActiveRecord::Base
  validates :name,  presence: true
  
  belongs_to :good
end