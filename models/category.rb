class Category < ActiveRecord::Base
  validates :title, presence: true, uniqueness: true
  
  has_many :goods
  
  scope  :subcategories, -> { where.not(parent_id: nil)}
  
end