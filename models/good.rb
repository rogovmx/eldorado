class Good < ActiveRecord::Base
  validates :title, presence: true
  validates :category_id, presence: true

  has_many :good_features
  belongs_to :category
  
  scope :found_in_yandex, -> {where(is_yandex_model: true)}
  scope :not_found_in_yandex, -> {where.not(is_yandex_model: true)}
  
    
  def self.dedupe
    grouped = all.group_by{|good| [good.foreign_id] }
    grouped.values.each do |duplicates|
      first_one = duplicates.shift
      duplicates.each{|double| double.destroy}
    end
  end
  
  
end