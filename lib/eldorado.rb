module Eldorado
  class Parser
    AUTH = "&oauth_token=c883ee85fe574b8d8d5d2ca62fd9b7e6&oauth_client_id=ac448773268a4ee49e954b09c779f319&oauth_login=eders1978"
    def initialize(options)
      @stores = {}
      Store.all.each{ |s| @stores[s.code] = s.id }
      @domain = "http://" + options[:domain]
      @start_url = @domain
      @cookies = options[:cookies]
    end

    def perform
      p '==================================== Eldorado ========================================='
      Category.subcategories.each do |s|
        subcategory = {id: s.id, url: s.url.sub(@domain, ''), title: s.title}
        p "Parsing goods data for #{subcategory[:title]}...  #{Time.now}"
        @goods, @qtys = [], []
        get_goods_info subcategory

        p "Saving goods...#{Time.now}"
        save_goods
      end
    
      p "Eldorado. Parsing done! #{Time.now}"
    end
    
    def parse_stores_categories
      p "Parsing stores... #{Time.now}"
      get_stores
      
      p "Parsing main categories... #{Time.now}"
      categories = get_categories
      
      p "Parsing subcategories... #{Time.now}"
      get_subcategories categories
    end
    
    def features
      get_goods_fetures
    end
    
    def scan_by_yandex_model
      p "Start scan by yandex Eldorado#{Time.now}"
      threads = []
      @goods = Good.not_found_in_yandex
      @goods.each_slice((Good.count / 2) + 1) do |goods|
#        threads << Thread.new do
          goods.each do |good|
            resp = get_models_from_yandex(good.title) || get_models_from_yandex(good.title.gsub(/\p{Cyrillic}/, '').strip) || get_models_from_yandex(good.short_name)    
            if resp
              good.is_yandex_model = true
              good.yandex_id = resp['models'].first['id'].to_s.to_i
              p "Query: #{@query}"
              p good.yandex_id 
              p good.id
            else
#              good.is_yandex_model = false
#              good.yandex_id = nil
            end 
            good.save
          end
#        end  
      end  
#      threads.each(&:join)
      p "Done scan by yandex (found: #{Good.found_in_yandex.count} from: #{Good.count}) #{Time.now}"
    end
    
    def save_goods_csv
      p "CSV goods start #{Time.now}"
      @goods = Good.where("goods.updated_at > ?", Time.now - 2.days)
      CSV.open("/var/www/ror/grabber_app/public/csv/eldorado_parse.csv", "wb") do |csv|
        csv << ["Категория", "Бренд", "Наименование", "Короткое наименование", "URL", "Ключ эльдорадо", "Цена", "Картинка", "YandexId"]
        @goods.each do |good|
          csv << [good.category.title, good.brend, good.title, good.short_name, good.url, good.foreign_id, good.price, good.image, good.yandex_id]
        end
      end
      p "CSV goods saved #{Time.now}"
    end
    
    def save_features_csv
      p "CSV features start #{Time.now}"
      @good_features = GoodFeature.includes(:good)
      CSV.open("/var/www/ror/grabber_app/public/csv/eldorado_goods_features.csv", "wb") do |csv|
        csv << ["id товара", "Ключ эльдорадо", "Характеристика", "Значение"]
        @good_features.each do |feature|
          csv << [feature.good_id, feature.good.foreign_id, feature.name, feature.sign]
        end
      end
      p "CSV features saved #{Time.now}"
    end

    def updating_prices
      p "Start updating prices... #{Time.now}"
      threads = []
        Good.all.each_slice(Good.all.size / 5) do |goods|
          threads << Thread.new do
            goods.each do |good|
              begin
                #doc = Nokogiri::HTML(open("http://www.eldorado.ru/_ajax/update_info_catalog.php?action=list&ids[]=5465764&ids[]=5465764", "Cookie" => @cookies), nil, 'utf-8')
                doc = Nokogiri::HTML(open(good.url, "Cookie" => @cookies), nil, 'utf-8')
                price = doc.css('.bigPriceContainer .itemPriceDecoratedBig .two .unifiedHidden').text.strip.to_i
                good.update_attribute(:price, price)
              rescue => error
                p "#{Time.now} error loading price #{good.url}"
                p error.inspect
              end  
            end  
          end
        end  
      threads.each(&:join)
      p "Updating prices done #{Time.now}"
    end

    #http://www.eldorado.ru/cat/SLSubPage.php?city_id=11279&mode=ajax&XID=71086490&content=listShopsContPickup
    def get_delivery_dates
      p "Start get_delivery_dates #{Time.now}"
      @goods = Good.all
      @goods.each do |good|
        delivery_link = "http://www.eldorado.ru/cat/SLSubPage.php?city_id=11279&mode=ajax&XID=#{good.foreign_id}&content=listShopsContPickup"
        delivery_doc = Nokogiri::HTML(open(delivery_link))
#        dates = delivery_doc.css('.date-delivery .self_how_much b').map { |date| Iconv.conv('utf-8', 'ISO-8859-1', date.children.text) }.uniq
#        ap dates
        dates = delivery_doc.css('.date-delivery .self_how_much')
#        ap dates
#        ap dates.css('b')
        dates ||= dates.css('b')
        dates = dates.map { |date| date.text }.uniq
        @dd = DeliveryDate.find(good.id) || DeliveryDate.new
        @dd.update_attributes(good_id: good.id, date: dates.to_s)
      end
      p "Done get_delivery_dates #{Time.now}"
    end
    
    
    private

    def get_stores
      url = @domain + "/info/shops/11279/"
      doc = Nokogiri::HTML(open(url, "Cookie" => @cookies))
      metros = doc.css('.metro p')
      doc.css('.shop-item .name a').each_with_index do |store, index|
        code = store['href'][store['href'][0..-2].rindex('/')+1..-2][/\d+$/].to_i
        title = store.text.strip + ', ' + metros[index].text.strip
        s = Store.create code: code, title: title
        p title
        @stores[s.code] = s.id
      end
    end

    def get_categories
      doc = Nokogiri::HTML(open(@domain, "Cookie" => @cookies))
      categories = doc.css('a.headerCatalogItemLink')
        .map { |cat| {title: cat.css('.text').text.strip, url: cat["href"]} }
        .reject { |cat| cat[:url] == "#" || cat[:url] == "/cat" }
      categories.each_with_index do |category, index|
        categories[index][:id] = Category.create(title: category[:title]).id
        #ap category
      end
      categories
    end

    def get_subcategories(categories)
      subcategories = []
      threads = []
      categories.each do |category|
        category_url = @domain + category[:url].sub("http://www.eldorado.ru", '')
        threads << Thread.new do
          begin
            doc = Nokogiri::HTML(open(category_url, "Cookie" => @cookies)) 
            subcategories << doc.css('a.catalogLvl2ItemHd')
              .map{|cat| {title: cat.text.strip, url: cat["href"], parent: category[:id]}}
              
            subcategories.last.delete_if do |subcategory|
              subcategory_url = @domain + subcategory[:url]
              doc = Nokogiri::HTML(open(subcategory_url, "Cookie" => @cookies)) 
              if doc.css('a.catalogLvl2ItemHd').any?
                subcategories << doc.css('a.catalogLvl2ItemHd')
                  .map{|cat| {title: cat.text.strip, url: cat["href"], parent: category[:id]}}
                true  
              end
            end 
            subcategories.last.delete_if { |s| s[:title] == 'Подарочные карты'}  
          rescue
            p "Error loading subcategoriy #{category_url}"
          end  
        end
      end
      threads.each(&:join)
      subcategories.flatten!.each_with_index do |category, index|
        subcategories[index][:id] = Category.create(title: category[:title], parent_id: category[:parent], url: @domain + category[:url]).id
      end
    end
    
    
    def get_goods_fetures
      p "Eldorado. Start goods_features #{Time.now}"
      GoodFeature.delete_all
      p "Delete all #{Time.now}"
      threads = []
      @goods = Good.all
      @goods.each_slice(Good.all.size / 5) do |goods|
        threads << Thread.new do
          goods.each do |good|
            begin    
              doc_good = Nokogiri::HTML(open(good.url, "Cookie" => @cookies))
              descr_link = doc_good.css('.detailTabsItem a')[2]['href'] if doc_good.css('.detailTabsItem a')
              if descr_link
                descr_doc = Nokogiri::HTML(open(@domain + descr_link, "Cookie" => @cookies))
                descr_doc_box = descr_doc.css('.specificationTextTable table tr')#[2..-1]
                descr_doc_box.each do |descr_tr|
                  name = descr_tr.css('td').first.children.text
                  sign = descr_tr.css('td')[1].text
                  good.good_features << GoodFeature.new(name: name, sign: sign) unless name.blank?
                end
              end  
            rescue  => error
              p "Error loading goods_features #{good.url}"
              p " #{error.inspect}"
            end  
          end
        end  
      end
      threads.each(&:join)
      p "Eldorado. Done goods_features #{Time.now}"
    end
    
    

    def get_goods_info(category, page = 1, pages = nil)
      p "page #{page}"
      page_url = @domain + category[:url] + "page/#{page}/"
      begin
        doc = Nokogiri::HTML(open(page_url, "Cookie" => @cookies))
        threads = []
        doc.css('.goodsList .item').each do |good|
          threads << Thread.new do
            title = good.css('.itemTitle a').text.strip
            good_url = @domain + good.css('.itemTitle a').first['href']
            
            doc_good = Nokogiri::HTML(open(good_url, "Cookie" => @cookies))
            
            foreign_id = doc_good.css(".article").text.sub('Арт.', '').strip.to_i
            brend = doc_good.css('.logo .detailBrandLogo img').first['alt'].sub('_', ' ') if doc_good.css('.logo .detailBrandLogo img').first
            image = good.css('.itemPicture .link img').first['src']
            price = good.css('.unifiedHidden').first.text.strip.sub(' ', '').to_i 
            bonus = doc_good.css('.bonusQuantity .num').text.to_s.strip.to_i 
            @goods << {title: title, price: price, image: image, category_id: category[:id], foreign_id: foreign_id, 
              url: good_url, bonus: bonus, brend: brend, short_name: title.gsub(/\p{Cyrillic}/, '').sub(brend, '').sub('.', ' ').strip}
          end
          threads.each(&:join)
        end
      rescue => error
        p "Error loading goods_info #{page_url}"
        p error.inspect
      end
      pages_count = pages || doc.css('.pages a.page').last.try(:text).try(:to_i) || 1
      get_goods_info(category, page + 1, pages_count) if pages_count > page
    end

    def save_goods
      @goods.uniq! { |good| good[:foreign_id]}
      foreigns = Good.pluck(:foreign_id)
      deleted = 
        @goods.delete_if do |good|
          foreigns.include?(good[:foreign_id])
        end
      Good.create @goods
      deleted.each do |good|
        g = Good.find_by(foreign_id: good[:foreign_id])
        g.update(good) if g
      end
#      Good.dedupe
    end
    
    def get_models_from_yandex query
      @url = "https://api.partner.market.yandex.ru/v2/models.json?regionId=2&query=#{URI.encode query}" + AUTH
      @query = query
      uri = URI.parse(@url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(request)
      resp = response.body
      File.open("log/models.log", 'a') do |file| 
        unless response.code == "200"
          file.write response.code;
          file.write response.body.inspect; 
          file.write Time.now; file.write "\n" 
          p response.body.inspect
        end  
      end
      resp = JSON.parse(resp)
      if resp && resp['pager'] && resp['pager']['total'] == 1
        resp
      else  
        false
      end
      
    rescue => error
      File.open("log/models.log", 'a') do |file| 
          file.write error.inspect 
          file.write Time.now; file.write "\n" 
      end
      p error.inspect 
      nil
    end
    
  end
end


